#!/bin/bash

GITLAB_API_URL="${GITLAB_URL}/api/v4"
DATE=`date --utc +%Y%m%d.%H%M.%S`
GITLAB_PROJECT_URLENCODED=${GITLAB_PROJECT_NAME/\//%2F}
GITLAB_PROJECT_ID=`curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_URLENCODED} | jq .id`
GITLAB_PROJECT_URL="${GITLAB_API_URL}/projects/${GITLAB_PROJECT_ID}"
PACKAGE_REGISTRY_URL="${GITLAB_PROJECT_URL}/packages/generic/zcdu_release/$DATE/"

mkdir -p target/assets
rm target/assets/release.json
echo -n "{ \"name\":\"Release ${VERSION}\", \"tag_name\":\"${VERSION}\", \"assets\":{ \"links\": [" >> target/assets/release.json
SEPARATOR=""

sed 's/${VERSION}/'"$VERSION"'/' src/setup-resources/FileDescriptions.txt | while read line; do 
    FILENAME=${line%%---*}; 
    BASENAME=$(basename -- "$FILENAME")
    DESCRIPTION=${line##*---}; 
    DESCRIPTION=${DESCRIPTION//[$'\t\r\n']}
    echo "${FILENAME}: ${DESCRIPTION}";
    FILE_RESULT=`curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --upload-file target/${FILENAME} ${PACKAGE_REGISTRY_URL}/${BASENAME}`;
    echo "Result: ${FILE_RESULT}";
    PACKAGE_ID=`curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${GITLAB_API_URL}/projects/${GITLAB_PROJECT_ID}/packages" | jq '.[] | select(.version=="'${DATE}'") | .id'`;
    CURL_OUT=`curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${GITLAB_API_URL}/projects/${GITLAB_PROJECT_ID}/packages/${PACKAGE_ID}/package_files"`;
#    echo "Package files: ${CURL_OUT}";
    FILE_ID=`echo ${CURL_OUT} | jq '.[] | select(.file_name=="'${BASENAME}'") | .id'`;
    DOWNLOAD_URL=${GITLAB_URL}/${GITLAB_PROJECT_NAME}/-/package_files/${FILE_ID}/download;
    echo "Package ID: ${PACKAGE_ID}, file ID: ${FILE_ID}, download URL: ${DOWNLOAD_URL}";
    ASSET=" --assets-link ";
    echo -n "${SEPARATOR}{\"name\":\"${DESCRIPTION}: ${BASENAME}\",\"url\":\"${DOWNLOAD_URL}\", \"type:\": \"other\"}" >> target/assets/release.json
    SEPARATOR=", "
done;

echo "]}}" >> target/assets/release.json
TAG_CREATION=`curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${GITLAB_PROJECT_URL}/repository/tags?tag_name=${VERSION}&ref=master"`
DATA=`cat target/assets/release.json`
echo "Data: ${DATA}"
RELEASE_CREATION=`curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --header "Content-Type: application/json" --data "${DATA}" --request POST ${GITLAB_PROJECT_URL}/releases`
