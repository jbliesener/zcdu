/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
//import java.util.logging.Logger;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.andreels.zcdu.CDU.CduScreen;



public class UI extends JFrame {

	private Image logo_image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("ZCDU_logo.png"));
	//private static Logger logger = Logger.getLogger("org.andreels.zcdu");

	private JPanel contentPane;
	private Color bgColor = new Color(128, 128, 128);

	JPanel btnSettings = new JPanel();
	JLayeredPane settingsPane = new JLayeredPane();
	JPanel btnExit = new JPanel();

	JLabel lblExtPStatus;
	JPanel menuBar;

	JTextField txtExtPlaneIP;
	JLabel lblSaveIP;
	JPanel btnSaveIP;

	CduScreen cpt_cdu = null;
	CduScreen fo_cdu = null;

	private ZCDUPreferences preferences;
	private DataFactory data_instance;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UI(DataFactory data_instance, String title) {
		this.data_instance = data_instance;
		this.preferences = ZCDUPreferences.getInstance();
		this.setIconImage(this.logo_image);
		setResizable(false);
		this.setTitle(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(bgColor);
		contentPane.setBorder(null);
		contentPane.setLayout(null);
		setContentPane(contentPane);
		setBounds(0, 0, 706, 490);
		settingsPane.setBackground(bgColor);
		settingsPane.setBorder(null);
		settingsPane.setBounds(0, 30, 706, 368);
		contentPane.add(settingsPane);
		menuBar = new JPanel();
		menuBar.setBackground(bgColor);
		menuBar.setBounds(0, 0, 706, 45);
		contentPane.add(menuBar);
		menuBar.setLayout(null);

		instructions();

		settingsPage();

		statusBar();

		buttons();

		//cpt_cdu = new CduScreen(data_instance, "CPT CDU");
		//fo_cdu = new CduScreen(data_instance, "FO CDU");
	}

	private void instructions() {
		JLabel lblInstructions = new JLabel();
		lblInstructions.setBounds(0, 0, 706, 40);
		lblInstructions.setText("<html><b><font color=\"white\">Instructions:</font> </b> Use <b>scroll wheel or arrow keys</b> to resize window. Save location: right click and choose \"<b>Save CDU Size and Location</b>\". Enter prefered font sizes and line locations below (Y-axis in pixels - hit Enter to save each one) </html>");
		menuBar.add(lblInstructions);
	}

	private void buttons() {

		btnSaveIP.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if(validIP(txtExtPlaneIP.getText())) {
					preferences.set_preference(ZCDUPreferences.PREF_EXTPLANE_SERVER, txtExtPlaneIP.getText());
					lblSaveIP.setForeground(Color.GREEN);
					lblSaveIP.setText("Saved!!");
					txtExtPlaneIP.setForeground(Color.BLACK);
				}else {
					//preferences.set_preference(ZHSIPreferences.PREF_EXTPLANE_SERVER, "");
					txtExtPlaneIP.setForeground(Color.RED);
					txtExtPlaneIP.setText("IP address not valid!");

				}
			}
		});

	}

	private void settingsPage() {

		JLabel lblExtPlaneIPAddress = new JLabel("X-Plane PC IP Address:");
		lblExtPlaneIPAddress.setBounds(15, 40, 195, 20);
		lblExtPlaneIPAddress.setForeground(new Color(0, 0, 0));
		lblExtPlaneIPAddress.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblExtPlaneIPAddress);

		txtExtPlaneIP = new JTextField();
		if(preferences.get_preference(ZCDUPreferences.PREF_EXTPLANE_SERVER).isEmpty()) {
			txtExtPlaneIP.setForeground(Color.RED);
			txtExtPlaneIP.setText("Enter valid IP Address");
		}else {
			txtExtPlaneIP.setText(preferences.get_preference(ZCDUPreferences.PREF_EXTPLANE_SERVER));
		}
		txtExtPlaneIP.setEditable(true);
		txtExtPlaneIP.setBounds(200, 40, 220, 18);
		settingsPane.add(txtExtPlaneIP);

		btnSaveIP = new JPanel();
		btnSaveIP.setForeground(new Color(255, 255, 255));
		btnSaveIP.setBackground(new Color(102, 153, 255));
		btnSaveIP.setBounds(440, 40, 120, 20);
		settingsPane.add(btnSaveIP);
		btnSaveIP.setLayout(new BorderLayout(0, 0));

		lblSaveIP = new JLabel("Save");
		lblSaveIP.setHorizontalAlignment(SwingConstants.CENTER);
		lblSaveIP.setAlignmentX(0.5f);
		lblSaveIP.setFont(new Font("Arial", Font.PLAIN, 16));
		lblSaveIP.setForeground(new Color(255, 255, 255));
		btnSaveIP.add(lblSaveIP);

		JLabel lblSettingsHeader= new JLabel("CDU Screen Settings");
		lblSettingsHeader.setBounds(15, 5, 400, 39);
		settingsPane.add(lblSettingsHeader);
		lblSettingsHeader.setForeground(new Color(255, 255, 255));
		lblSettingsHeader.setFont(new Font("Tahoma", Font.BOLD, 22));

		checkboxes();

		cptCDUSettings();

		foCDUSettings();

	}

	private void cptCDUSettings() {
		JLabel lblcpt_L_font = new JLabel("Large Font Size:");
		lblcpt_L_font.setBounds(15, 120, 195, 20);
		lblcpt_L_font.setForeground(new Color(0, 0, 0));
		lblcpt_L_font.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_L_font);
		JLabel lblcpt_S_font = new JLabel("Small Font Size:");
		lblcpt_S_font.setBounds(15, 145, 195, 20);
		lblcpt_S_font.setForeground(new Color(0, 0, 0));
		lblcpt_S_font.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_S_font);
		JLabel lblcpt_line00_y = new JLabel("Line00:");
		lblcpt_line00_y.setBounds(15, 170, 195, 20);
		lblcpt_line00_y.setForeground(new Color(0, 0, 0));
		lblcpt_line00_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line00_y);
		JLabel lblcpt_line01_y = new JLabel("Line01:");
		lblcpt_line01_y.setBounds(15, 195, 195, 20);
		lblcpt_line01_y.setForeground(new Color(0, 0, 0));
		lblcpt_line01_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line01_y);
		JLabel lblcpt_line02_y = new JLabel("Line02:");
		lblcpt_line02_y.setBounds(15, 220, 195, 20);
		lblcpt_line02_y.setForeground(new Color(0, 0, 0));
		lblcpt_line02_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line02_y);
		JLabel lblcpt_line03_y = new JLabel("Line03:");
		lblcpt_line03_y.setBounds(15, 245, 195, 20);
		lblcpt_line03_y.setForeground(new Color(0, 0, 0));
		lblcpt_line03_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line03_y);
		JLabel lblcpt_line04_y = new JLabel("Line04:");
		lblcpt_line04_y.setBounds(15, 270, 195, 20);
		lblcpt_line04_y.setForeground(new Color(0, 0, 0));
		lblcpt_line04_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line04_y);
		JLabel lblcpt_line05_y = new JLabel("Line05:");
		lblcpt_line05_y.setBounds(15, 295, 195, 20);
		lblcpt_line05_y.setForeground(new Color(0, 0, 0));
		lblcpt_line05_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line05_y);
		JLabel lblcpt_line06_y = new JLabel("Line06:");
		lblcpt_line06_y.setBounds(15, 320, 195, 20);
		lblcpt_line06_y.setForeground(new Color(0, 0, 0));
		lblcpt_line06_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_line06_y);
		JLabel lblcpt_scratch_y = new JLabel("ScratchPad:");
		lblcpt_scratch_y.setBounds(15, 345, 195, 20);
		lblcpt_scratch_y.setForeground(new Color(0, 0, 0));
		lblcpt_scratch_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblcpt_scratch_y);
		////
		////
		JTextField txtCpt_L_font = new JTextField();
		txtCpt_L_font.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L_FONT_SIZE));
		txtCpt_L_font.setEditable(true);
		txtCpt_L_font.setBounds(150, 120, 50, 18);
		settingsPane.add(txtCpt_L_font);
		txtCpt_L_font.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L_FONT_SIZE, txtCpt_L_font.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_S_font = new JTextField();
		txtCpt_S_font.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_S_FONT_SIZE));
		txtCpt_S_font.setEditable(true);
		txtCpt_S_font.setBounds(150, 145, 50, 18);
		settingsPane.add(txtCpt_S_font);
		txtCpt_S_font.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_S_FONT_SIZE, txtCpt_S_font.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l00_y = new JTextField();
		txtCpt_l00_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L00_Y));
		txtCpt_l00_y.setEditable(true);
		txtCpt_l00_y.setBounds(150, 170, 50, 18);
		settingsPane.add(txtCpt_l00_y);
		txtCpt_l00_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L00_Y, txtCpt_l00_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l01_y = new JTextField();
		txtCpt_l01_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L01_Y));
		txtCpt_l01_y.setEditable(true);
		txtCpt_l01_y.setBounds(150, 195, 50, 18);
		settingsPane.add(txtCpt_l01_y);
		txtCpt_l01_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L01_Y, txtCpt_l01_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l02_y = new JTextField();
		txtCpt_l02_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L02_Y));
		txtCpt_l02_y.setEditable(true);
		txtCpt_l02_y.setBounds(150, 220, 50, 18);
		settingsPane.add(txtCpt_l02_y);
		txtCpt_l02_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L02_Y, txtCpt_l02_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l03_y = new JTextField();
		txtCpt_l03_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L03_Y));
		txtCpt_l03_y.setEditable(true);
		txtCpt_l03_y.setBounds(150, 245, 50, 18);
		settingsPane.add(txtCpt_l03_y);
		txtCpt_l03_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L03_Y, txtCpt_l03_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l04_y = new JTextField();
		txtCpt_l04_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L04_Y));
		txtCpt_l04_y.setEditable(true);
		txtCpt_l04_y.setBounds(150, 270, 50, 18);
		settingsPane.add(txtCpt_l04_y);
		txtCpt_l04_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L04_Y, txtCpt_l04_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l05_y = new JTextField();
		txtCpt_l05_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L05_Y));
		txtCpt_l05_y.setEditable(true);
		txtCpt_l05_y.setBounds(150, 295, 50, 18);
		settingsPane.add(txtCpt_l05_y);
		txtCpt_l05_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L05_Y, txtCpt_l05_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_l06_y = new JTextField();
		txtCpt_l06_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L06_Y));
		txtCpt_l06_y.setEditable(true);
		txtCpt_l06_y.setBounds(150, 320, 50, 18);
		settingsPane.add(txtCpt_l06_y);
		txtCpt_l06_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L06_Y, txtCpt_l06_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtCpt_scratch_y = new JTextField();
		txtCpt_scratch_y.setText(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_SCRATCH_Y));
		txtCpt_scratch_y.setEditable(true);
		txtCpt_scratch_y.setBounds(150, 345, 50, 18);
		settingsPane.add(txtCpt_scratch_y);
		txtCpt_scratch_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_SCRATCH_Y, txtCpt_scratch_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});

	}

	private void foCDUSettings() {
		JLabel lblfo_L_font = new JLabel("Large Font Size:");
		lblfo_L_font.setBounds(350, 120, 195, 20);
		lblfo_L_font.setForeground(new Color(0, 0, 0));
		lblfo_L_font.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_L_font);
		JLabel lblfo_S_font = new JLabel("Small Font Size:");
		lblfo_S_font.setBounds(350, 145, 195, 20);
		lblfo_S_font.setForeground(new Color(0, 0, 0));
		lblfo_S_font.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_S_font);
		JLabel lblfo_line00_y = new JLabel("Line00:");
		lblfo_line00_y.setBounds(350, 170, 195, 20);
		lblfo_line00_y.setForeground(new Color(0, 0, 0));
		lblfo_line00_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line00_y);
		JLabel lblfo_line01_y = new JLabel("Line01:");
		lblfo_line01_y.setBounds(350, 195, 195, 20);
		lblfo_line01_y.setForeground(new Color(0, 0, 0));
		lblfo_line01_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line01_y);
		JLabel lblfo_line02_y = new JLabel("Line02:");
		lblfo_line02_y.setBounds(350, 220, 195, 20);
		lblfo_line02_y.setForeground(new Color(0, 0, 0));
		lblfo_line02_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line02_y);
		JLabel lblfo_line03_y = new JLabel("Line03:");
		lblfo_line03_y.setBounds(350, 245, 195, 20);
		lblfo_line03_y.setForeground(new Color(0, 0, 0));
		lblfo_line03_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line03_y);
		JLabel lblfo_line04_y = new JLabel("Line04:");
		lblfo_line04_y.setBounds(350, 270, 195, 20);
		lblfo_line04_y.setForeground(new Color(0, 0, 0));
		lblfo_line04_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line04_y);
		JLabel lblfo_line05_y = new JLabel("Line05:");
		lblfo_line05_y.setBounds(350, 295, 195, 20);
		lblfo_line05_y.setForeground(new Color(0, 0, 0));
		lblfo_line05_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line05_y);
		JLabel lblfo_line06_y = new JLabel("Line06:");
		lblfo_line06_y.setBounds(350, 320, 195, 20);
		lblfo_line06_y.setForeground(new Color(0, 0, 0));
		lblfo_line06_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_line06_y);
		JLabel lblfo_scratch_y = new JLabel("ScratchPad:");
		lblfo_scratch_y.setBounds(350, 345, 195, 20);
		lblfo_scratch_y.setForeground(new Color(0, 0, 0));
		lblfo_scratch_y.setFont(new Font("Tahoma", Font.PLAIN, 16));
		settingsPane.add(lblfo_scratch_y);
		////
		////
		JTextField txtFo_L_font = new JTextField();
		txtFo_L_font.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L_FONT_SIZE));
		txtFo_L_font.setEditable(true);
		txtFo_L_font.setBounds(485, 120, 50, 18);
		settingsPane.add(txtFo_L_font);
		txtFo_L_font.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L_FONT_SIZE, txtFo_L_font.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_S_font = new JTextField();
		txtFo_S_font.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_S_FONT_SIZE));
		txtFo_S_font.setEditable(true);
		txtFo_S_font.setBounds(485, 145, 50, 18);
		settingsPane.add(txtFo_S_font);
		txtFo_S_font.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_S_FONT_SIZE, txtFo_S_font.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l00_y = new JTextField();
		txtFo_l00_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L00_Y));
		txtFo_l00_y.setEditable(true);
		txtFo_l00_y.setBounds(485, 170, 50, 18);
		settingsPane.add(txtFo_l00_y);
		txtFo_l00_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L00_Y, txtFo_l00_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l01_y = new JTextField();
		txtFo_l01_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L01_Y));
		txtFo_l01_y.setEditable(true);
		txtFo_l01_y.setBounds(485, 195, 50, 18);
		settingsPane.add(txtFo_l01_y);
		txtFo_l01_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L01_Y, txtFo_l01_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l02_y = new JTextField();
		txtFo_l02_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L02_Y));
		txtFo_l02_y.setEditable(true);
		txtFo_l02_y.setBounds(485, 220, 50, 18);
		settingsPane.add(txtFo_l02_y);
		txtFo_l02_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L02_Y, txtFo_l02_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l03_y = new JTextField();
		txtFo_l03_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L03_Y));
		txtFo_l03_y.setEditable(true);
		txtFo_l03_y.setBounds(485, 245, 50, 18);
		settingsPane.add(txtFo_l03_y);
		txtFo_l03_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L03_Y, txtFo_l03_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l04_y = new JTextField();
		txtFo_l04_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L04_Y));
		txtFo_l04_y.setEditable(true);
		txtFo_l04_y.setBounds(485, 270, 50, 18);
		settingsPane.add(txtFo_l04_y);
		txtFo_l04_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L04_Y, txtFo_l04_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l05_y = new JTextField();
		txtFo_l05_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L05_Y));
		txtFo_l05_y.setEditable(true);
		txtFo_l05_y.setBounds(485, 295, 50, 18);
		settingsPane.add(txtFo_l05_y);
		txtFo_l05_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L05_Y, txtFo_l05_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_l06_y = new JTextField();
		txtFo_l06_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L06_Y));
		txtFo_l06_y.setEditable(true);
		txtFo_l06_y.setBounds(485, 320, 50, 18);
		settingsPane.add(txtFo_l06_y);
		txtFo_l06_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L06_Y, txtFo_l06_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});
		////
		////
		JTextField txtFo_scratch_y = new JTextField();
		txtFo_scratch_y.setText(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_SCRATCH_Y));
		txtFo_scratch_y.setEditable(true);
		txtFo_scratch_y.setBounds(485, 345, 50, 18);
		settingsPane.add(txtFo_scratch_y);
		txtFo_scratch_y.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_SCRATCH_Y, txtFo_scratch_y.getText());
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

		});

	}

	private void checkboxes() {
		JCheckBox chckbxCaptainCDUScreen = new JCheckBox("Captain CDU Screen");

		chckbxCaptainCDUScreen.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					if (cpt_cdu == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									cpt_cdu =  new CduScreen(data_instance, "CPT CDU");
									cpt_cdu.setVisible(true);
									preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
					chckbxCaptainCDUScreen.setForeground(Color.GREEN);
				}else {
					if (cpt_cdu != null) {
						cpt_cdu.dispose();
						//cptoutbd_heartbeat.stop();
						cpt_cdu = null;
						preferences.set_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_ENABLE, "false");
						chckbxCaptainCDUScreen.setForeground(Color.BLACK);
					}
				}
			}

		});
		if(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_ENABLE).equals("true")) {
			chckbxCaptainCDUScreen.setSelected(true);
			chckbxCaptainCDUScreen.setForeground(Color.GREEN);
		}else {
			chckbxCaptainCDUScreen.setSelected(false);
			chckbxCaptainCDUScreen.setForeground(Color.BLACK);
		}
		chckbxCaptainCDUScreen.setBackground(bgColor);
		chckbxCaptainCDUScreen.setBounds(15, 80, 245, 29);
		chckbxCaptainCDUScreen.setFont(new Font("Tahoma", Font.PLAIN, 16));

		settingsPane.add(chckbxCaptainCDUScreen);


		JCheckBox chckbxFoCDUScreen = new JCheckBox("First Officer CDU Screen");

		chckbxFoCDUScreen.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					if (fo_cdu == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									fo_cdu =  new CduScreen(data_instance, "FO CDU");
									fo_cdu.setVisible(true);
									preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
					chckbxFoCDUScreen.setForeground(Color.GREEN);
				}else {
					if (fo_cdu != null) {
						fo_cdu.dispose();
						//cptoutbd_heartbeat.stop();
						fo_cdu = null;
						preferences.set_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_ENABLE, "false");
						chckbxFoCDUScreen.setForeground(Color.BLACK);
					}
				}
			}

		});
		if(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_ENABLE).equals("true")) {
			chckbxFoCDUScreen.setSelected(true);
			chckbxFoCDUScreen.setForeground(Color.GREEN);
		}else {
			chckbxFoCDUScreen.setSelected(false);
			chckbxFoCDUScreen.setForeground(Color.BLACK);
		}
		chckbxFoCDUScreen.setBackground(bgColor);
		chckbxFoCDUScreen.setBounds(350, 80, 245, 29);
		chckbxFoCDUScreen.setFont(new Font("Tahoma", Font.PLAIN, 16));

		settingsPane.add(chckbxFoCDUScreen);

	}

	private void statusBar() {

		JPanel statusBar = new JPanel();
		statusBar.setBackground(new Color(0, 0, 0));
		statusBar.setBounds(0, 400, 706, 50);
		contentPane.add(statusBar);
		statusBar.setLayout(null);

		JLabel lblExtPStatusText = new JLabel("XPlane Status");
		lblExtPStatusText.setForeground(new Color(255, 255, 255));
		lblExtPStatusText.setBounds(1, 5, 100, 16);
		statusBar.add(lblExtPStatusText);

		lblExtPStatus = new JLabel("Not Connected");
		lblExtPStatus.setForeground(Color.RED);
		lblExtPStatus.setBounds(90, 5, 400, 16);
		statusBar.add(lblExtPStatus);


	}

	public void heartbeat() {

		if(txtExtPlaneIP.getText().equals(preferences.get_preference(ZCDUPreferences.PREF_EXTPLANE_SERVER))) {
			lblSaveIP.setForeground(Color.WHITE);
			lblSaveIP.setText("Save");
		}
		if (ZCDUStatus.receiving) {
			lblExtPStatus.setForeground(Color.GREEN);
		}else {
			lblExtPStatus.setForeground(Color.RED);
		}
		lblExtPStatus.setText(ZCDUStatus.cStatus);

	}


	private boolean validIP (String ip) {
		try {
			if ( ip == null || ip.isEmpty() ) {
				return false;
			}

			String[] parts = ip.split( "\\." );
			if ( parts.length != 4 ) {
				return false;
			}

			for ( String s : parts ) {
				int i = Integer.parseInt( s );
				if ( (i < 0) || (i > 255) ) {
					return false;
				}
			}
			if ( ip.endsWith(".") ) {
				return false;
			}

			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

}
