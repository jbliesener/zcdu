package org.andreels.zcdu.xpdata;

import java.util.Base64;

import org.andreels.zcdu.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRef;
import org.andreels.zcdu.ExtPlaneInterface.util.ArrayUtils;
import org.andreels.zcdu.ExtPlaneInterface.util.Observer;



/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els 
 */

public class XPDataRepository {
	
	private final String STATUS = "sim/aircraft/view/acf_tailnum";

	ExtPlaneInterface iface;
	private Observer<DataRef> status;

	
	public CDU cdu;
	
	private static XPDataRepository instance;
	

	private XPDataRepository(){
		this.iface = ExtPlaneInterface.getInstance();
		this.cdu = new CDU(this.iface);
		subscribeZibo();
	}

	public void reconnect() {
		cdu.clearData();
		subscribeZibo();
	}
	
	private void subscribeZibo() {
		status = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				if(object.getName().equals(STATUS)) {
					if (object.getValue().length == 0) 
						return;
						
					byte[] statusByte = Base64.getDecoder().decode(object.getValue()[0]);
					String decodedString = new String(ArrayUtils.trimNul(statusByte));
					if(decodedString.equals("ZB738")) {
						//zibo loaded
						includeDatarefs();
					}else {
						//zibo not loaded

					}
				}	
			}
		};
		iface.includeDataRef(STATUS);
		iface.observeDataRef(STATUS, status);
		
	}
	
	private void includeDatarefs() {
		this.cdu.subscribeDrefs();
	}

	static {
		instance = new XPDataRepository();
	}
	public static XPDataRepository getInstance() {
		return instance;
	}

}




