package org.andreels.zcdu.ExtPlaneInterface.comms;

import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.andreels.zcdu.ZCDUStatus;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRef;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRefFactory;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRefRepository;
import org.andreels.zcdu.ExtPlaneInterface.util.ObservableAware;


/**
 * 
 * Copyright (C) 2015  Pau G.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Pau G.
 */
public class InputHandler implements Runnable {

	private static Logger logger = Logger.getLogger("org.andreels.zcdu");
	private static final String DATA_REF_PATTERN = "^u(i|f|d|ia|fa|b)\\s.+";
	private static final String VERSION_PATTERN = "^EXTPLANE\\s.*";

	private final String data;
	private final DataRefRepository repository;

	private DataRef dataRef;
	//private ExtPlaneInterface iface = ExtPlaneInterface.get_instance();


	InputHandler(DataRefRepository repository, String data) {
		this.data = data;
		this.repository = repository;
	}

	public void run() {


		
		if(isDataRef(data)) {
			
			DataRefFactory drf = new DataRefFactory(data);

			dataRef = repository.getDataRef(drf.getDataRefName());
			dataRef = drf.getInstance(dataRef);
			repository.setDataRef(dataRef);

			ObservableAware.getInstance().update(dataRef);
		
			logger.finest(dataRef.toString());

		} else if(isVersion(data)) {
			logger.info("ExtPlane Plugin Version " + data.replace("EXTPLANE ", ""));
			ZCDUStatus.cStatus = "<html><font color=\"lime\">Connected to</font> ExtPlane Plugin <font color=\"white\">version</font> " + data.replace("EXTPLANE ", "") + "</html>";
		}


	}

	private boolean isDataRef(String data) {
		return Pattern.matches(InputHandler.DATA_REF_PATTERN, data);
	}

	private boolean isVersion(String data) {
		return Pattern.matches(InputHandler.VERSION_PATTERN, data);
	}
}
