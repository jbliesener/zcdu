package org.andreels.zcdu.ExtPlaneInterface;

import java.util.logging.Logger;

import org.andreels.zcdu.ZCDUPreferences;
import org.andreels.zcdu.ZCDUStatus;
import org.andreels.zcdu.ExtPlaneInterface.command.CommandMessage;
import org.andreels.zcdu.ExtPlaneInterface.command.DataRefCommand;
import org.andreels.zcdu.ExtPlaneInterface.command.ExtPlaneCommand;
import org.andreels.zcdu.ExtPlaneInterface.comms.ExtPlaneTCPReceiver;
import org.andreels.zcdu.ExtPlaneInterface.comms.ExtPlaneTCPSender;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRef;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRefRepository;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRefRepositoryImpl;
import org.andreels.zcdu.ExtPlaneInterface.data.MessageRepository;
import org.andreels.zcdu.ExtPlaneInterface.data.MessageRepositoryImpl;
import org.andreels.zcdu.ExtPlaneInterface.util.Constants.DataType;
import org.andreels.zcdu.ExtPlaneInterface.util.ObservableAware;
import org.andreels.zcdu.ExtPlaneInterface.util.Observer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * 
 * Copyright (C) 2015  Pau G.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Pau G.
 */
public class ExtPlaneInterface {

	private static Logger logger = Logger.getLogger("org.andreels.zcdu");

	private Socket socket;

	private DataRefRepository dataRefrepository;
	private MessageRepository messageRepository;

	private ExtPlaneTCPReceiver receive = null;
	private ExtPlaneTCPSender sender = null;

	private static ExtPlaneInterface instance;
	
	private static ZCDUPreferences preferences = ZCDUPreferences.getInstance();

	private String server;
	private int port = 51000;
	private int poolSize = 2;


	private ExtPlaneInterface() {
		this.server = preferences.get_preference(ZCDUPreferences.PREF_EXTPLANE_SERVER);

//		this.server = server;
//		this.port = port;

		this.initDataRefRepository();
		this.initMessageRepository();

	}

	public void excludeDataRef(String dataRefName) {
		this.sendMessage(new DataRefCommand(DataRefCommand.DATAREF_ACTION.UNSUBSCRIBE,dataRefName));
	}

	public DataRef getDataRef(String dataRef) {
		return this.dataRefrepository.getDataRef(dataRef);
	}

	public DataType getDataRefType(String dataRefName) {
		DataRef dr = this.dataRefrepository.getDataRef(dataRefName);

		if(dr!=null) 
			return dr.getDataType();

		return null;
	}

	public String[] getDataRefValue(String dataRefName) {
		DataRef dr = this.dataRefrepository.getDataRef(dataRefName);

		if(dr!=null) 
			return dr.getValue();


		return null;
	}

	public void includeDataRef(String dataRefName) {
		this.includeDataRef(dataRefName, null);
	}

	public void includeDataRef(String dataRefName, Float accuracy) {
		DataRefCommand drc = new DataRefCommand(DataRefCommand.DATAREF_ACTION.SUBSCRIBE,dataRefName);

		if(accuracy!=null)
			drc.setAccuracy(accuracy);

		this.sendMessage(drc);
	}

	public void setDataRefValue(String dataRefName, String... value) {
		this.sendMessage(new DataRefCommand(DataRefCommand.DATAREF_ACTION.SET, dataRefName, value));
	}

	public void sendMessage(CommandMessage message) {
		this.messageRepository.sendMessage(message);
	}

	public void setExtPlaneUpdateInterval(String interval) {
		this.sendMessage(new ExtPlaneCommand(ExtPlaneCommand.EXTPLANE_SETTING.UPDATE_INTERVAL, interval));
	}

	public void start() throws Exception {
		try {
			this.connect();
			this.startSending();
			this.startReceiving();
		} catch(Exception e) {
			logger.severe("Error starting services.");
			this.stopReceiving();
			this.stopSending();
			throw e;
		}
	}

	public void stop() {
		this.stopReceiving();
		this.stopSending();
	}

	public void observeDataRef(String dataRefName, Observer<DataRef> observer) {
		ObservableAware.getInstance().addObserver(dataRefName, observer);
	}

	public void unObserveDataRef(String dataRefName, Observer<DataRef> observer) {
		ObservableAware.getInstance().removeObserver(dataRefName, observer);
	}

	private void connect() throws InterruptedException {

		while(true) {
			try {
				//socket = new Socket(server, port);
				socket = new Socket();
				this.server = preferences.get_preference(ZCDUPreferences.PREF_EXTPLANE_SERVER);
				socket.connect(new InetSocketAddress(this.server, port), 2000);
				logger.info("Connected to ExtPlane plugin, Server=" + server + " port=" + port);
				ZCDUStatus.status = ZCDUStatus.STATUS_CONNECTED;
				ZCDUStatus.cStatus = "<html><font color=\"lime\">Connected to</font> " + server + " <font color=\"white\">port</font> " + port + "</html>";
				break;
			} catch (UnknownHostException e) {
				ZCDUStatus.status = ZCDUStatus.STATUS_NOT_CONNECTED;
				logger.fine("Unkown Host : Error connecting to " + server + " " + e.toString());
				ZCDUStatus.receiving = false;
				ZCDUStatus.cStatus = "<html><font color=\"red\">Unkown Host:</font><font color=\"white\"> Error connecting to:</font> " + server + "</html>";
				Thread.sleep(5000);
			} catch (IOException e) {
				ZCDUStatus.status = ZCDUStatus.STATUS_NOT_CONNECTED;
				logger.info("[ExtPlane] Error connecting to host " + server);
				ZCDUStatus.receiving = false;
				ZCDUStatus.cStatus = "<html><font color=\"red\">Error</font><font color=\"white\"> connecting to host:</font> " + server + "</html>";
				Thread.sleep(5000);
			}
		}  
	}

	private void initDataRefRepository() {
		this.dataRefrepository = new DataRefRepositoryImpl();
	}

	private void initMessageRepository() {
		this.messageRepository = new MessageRepositoryImpl();
	}

	private void startReceiving() {
		receive = new ExtPlaneTCPReceiver(socket, dataRefrepository, poolSize);
		receive.start();
		ZCDUStatus.receiving = true;
	}

	private void startSending() {
		sender = new ExtPlaneTCPSender(socket, messageRepository);
		sender.start();
	}

	private void stopReceiving() {
		if(this.receive!=null) 
			this.receive.setKeep_running(false);

		this.receive = null;
		ZCDUStatus.receiving = false;
	}

	private void stopSending() {
		if(this.sender!=null) 
			this.sender.setKeep_running(false);

		this.sender = null;
	}
	
	static {
		//instance = new ExtPlaneInterface(preferences.get_preference(ZHSIPreferences.PREF_EXTPLANE_SERVER), 51000);
		instance = new ExtPlaneInterface();
	}
	public static ExtPlaneInterface getInstance() {
		return instance;
	}

}
