/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu.resources;

import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import javax.swing.JPanel;



public class LoadResources extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static LoadResources instance;

	private static Logger logger = Logger.getLogger("org.andreels.zcdu");

	public Font CDUFont;
	public InputStream glassFrontStream;

	//gauges
	public BufferedImage img_flapGauge_bg;
	public BufferedImage img_L_needle;
	public BufferedImage img_R_needle;
	
	private LoadResources() {

		try {

			glassFrontStream = getClass().getResourceAsStream("/org/andreels/zcdu/resources/BCDU.otf");
			CDUFont = Font.createFont(Font.TRUETYPE_FONT, glassFrontStream);

		} catch (IOException e) {
			e.printStackTrace();
		} catch(FontFormatException e) {
			e.printStackTrace();
		}

		logger.info("Loading Resources");
//		try {
//
//			img_flapGauge_bg = ImageIO.read(getClass().getResource("/org/andreels/zhsi/resources/gauges/img_flapGauge_bg.png"));
//			img_L_needle = ImageIO.read(getClass().getResource("/org/andreels/zhsi/resources/gauges/img_L_needle.png"));
//			img_R_needle = ImageIO.read(getClass().getResource("/org/andreels/zhsi/resources/gauges/img_R_needle.png"));
//
//			logger.info("Resources loaded");
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	static {
		instance = new LoadResources();
	}
	public static LoadResources getInstance() {
		return instance;
	}
}