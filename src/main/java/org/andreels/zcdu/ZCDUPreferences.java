/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * Inspired by XHSI (http://xhsi.sourceforge.net)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class ZCDUPreferences {

	private static final String PROPERTY_FILENAME = System.getProperty("user.home")+"/.zcdu/ZCDU.properties";
	
	public static final String PREF_EXTPLANE_SERVER = "extplane.ip";
	public static final String PREF_LOGLEVEL = "loglevel";

	public static final String PREF_CPT_CDU_SCREEN_ENABLE = "cpt.cdu.screen.enable";
	public static final String PREF_CPT_CDU_SCREEN_POSX = "cpt.cdu.screen.posx";
	public static final String PREF_CPT_CDU_SCREEN_POSY = "cpt.cdu.screen.posy";
	public static final String PREF_CPT_CDU_SCREEN_WIDTH = "cpt.cdu.screen.width";
	public static final String PREF_CPT_CDU_SCREEN_HEIGHT = "cpt.cdu.screen.height";
	public static final String PREF_CPT_CDU_SCREEN_ALWAYS_ON_TOP = "cpt.cdu.screen.alwaystop";
	
	public static final String PREF_CPT_CDU_SCREEN_L_FONT_SIZE = "cpt.cdu.screen.lfont.size";
	public static final String PREF_CPT_CDU_SCREEN_S_FONT_SIZE = "cpt.cdu.screen.sfont.size";
	public static final String PREF_CPT_CDU_SCREEN_L00_Y = "cpt.cdu.screen.l00.y";
	public static final String PREF_CPT_CDU_SCREEN_L01_Y = "cpt.cdu.screen.l01.y";
	public static final String PREF_CPT_CDU_SCREEN_L02_Y = "cpt.cdu.screen.l02.y";
	public static final String PREF_CPT_CDU_SCREEN_L03_Y = "cpt.cdu.screen.l03.y";
	public static final String PREF_CPT_CDU_SCREEN_L04_Y = "cpt.cdu.screen.l04.y";
	public static final String PREF_CPT_CDU_SCREEN_L05_Y = "cpt.cdu.screen.l05.y";
	public static final String PREF_CPT_CDU_SCREEN_L06_Y = "cpt.cdu.screen.l06.y";
	public static final String PREF_CPT_CDU_SCREEN_SCRATCH_Y = "cpt.cdu.screen.scratch.y";
	

	public static final String PREF_FO_CDU_SCREEN_ENABLE = "fo.cdu.screen.enable";
	public static final String PREF_FO_CDU_SCREEN_POSX = "fo.cdu.screen.posx";
	public static final String PREF_FO_CDU_SCREEN_POSY = "fo.cdu.screen.posy";
	public static final String PREF_FO_CDU_SCREEN_WIDTH = "fo.cdu.screen.width";
	public static final String PREF_FO_CDU_SCREEN_HEIGHT = "fo.cdu.screen.height";
	public static final String PREF_FO_CDU_SCREEN_ALWAYS_ON_TOP = "fo.cdu.screen.alwaystop";
	
	public static final String PREF_FO_CDU_SCREEN_L_FONT_SIZE = "fo.cdu.screen.lfont.size";
	public static final String PREF_FO_CDU_SCREEN_S_FONT_SIZE = "fo.cdu.screen.sfont.size";
	public static final String PREF_FO_CDU_SCREEN_L00_Y = "fo.cdu.screen.l00.y";
	public static final String PREF_FO_CDU_SCREEN_L01_Y = "fo.cdu.screen.l01.y";
	public static final String PREF_FO_CDU_SCREEN_L02_Y = "fo.cdu.screen.l02.y";
	public static final String PREF_FO_CDU_SCREEN_L03_Y = "fo.cdu.screen.l03.y";
	public static final String PREF_FO_CDU_SCREEN_L04_Y = "fo.cdu.screen.l04.y";
	public static final String PREF_FO_CDU_SCREEN_L05_Y = "fo.cdu.screen.l05.y";
	public static final String PREF_FO_CDU_SCREEN_L06_Y = "fo.cdu.screen.l06.y";
	public static final String PREF_FO_CDU_SCREEN_SCRATCH_Y = "fo.cdu.screen.scratch.y";


	// private static ZHSIPreferences single_instance = null;
	private static ZCDUPreferences instance;
	private Properties preferences;
	private boolean unsaved_changes;

	private static Logger logger = Logger.getLogger("org.andreels.zcdu");

	/**
	 * Attempts to load preferences file. In case no preferences file can be found,
	 * a new preferences file with default values is created.
	 */
	private ZCDUPreferences() {
		this.preferences = new Properties();
		this.unsaved_changes = false;
		create_preferences_dir();
		load_preferences();
		ensure_preferences_complete();
		validate_preferences();
	}

	private void create_preferences_dir() {
		try {
			File f = new File(PROPERTY_FILENAME);
			File dir = f.getParentFile();
			if (!dir.exists()) {
				logger.info("Creating preferences directory " + dir.getCanonicalPath());
				if (!dir.mkdirs()) {
					logger.warning("Could not create preferences directory");
				}
			}
		} catch (Exception e) {
			logger.warning("Exception creating preferences directory: " + e.toString());
		}
	}

	private void load_preferences() {

		logger.fine("Reading " + PROPERTY_FILENAME);
		try {
			FileInputStream fis = new FileInputStream(PROPERTY_FILENAME);
			this.preferences.load(fis);
			if (fis != null)
				fis.close();
		} catch (IOException e) {
			logger.warning("Could not read properties file. Creating a new property file with default values ("
					+ e.toString() + ") ... ");
		}
	}

	private void ensure_preferences_complete() {

		if (preferences == null)
			throw new RuntimeException("Prefereces object not initialized!");
		// SYSTEM

		if (!this.preferences.containsKey(PREF_EXTPLANE_SERVER)) {
			this.preferences.setProperty(PREF_EXTPLANE_SERVER, "127.0.0.1");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_LOGLEVEL)) {
			this.preferences.setProperty(PREF_LOGLEVEL, "CONFIG");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_ENABLE)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_ENABLE, "false");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_POSX)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_POSX, "300");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_POSY)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_POSY, "300");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_WIDTH)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_WIDTH, "520");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_HEIGHT)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_HEIGHT, "480");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_ALWAYS_ON_TOP)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_ALWAYS_ON_TOP, "false");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L_FONT_SIZE)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L_FONT_SIZE, "24");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_S_FONT_SIZE)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_S_FONT_SIZE, "18");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L00_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L00_Y, "40");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L01_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L01_Y, "100");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L02_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L02_Y, "160");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L03_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L03_Y, "220");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L04_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L04_Y, "280");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L05_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L05_Y, "340");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_L06_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_L06_Y, "400");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_CPT_CDU_SCREEN_SCRATCH_Y)) {
			this.preferences.setProperty(PREF_CPT_CDU_SCREEN_SCRATCH_Y, "460");
			this.unsaved_changes = true;
		}
		
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_ENABLE)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_ENABLE, "false");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_POSX)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_POSX, "400");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_POSY)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_POSY, "400");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_WIDTH)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_WIDTH, "520");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_HEIGHT)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_HEIGHT, "480");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_ALWAYS_ON_TOP)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_ALWAYS_ON_TOP, "false");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L_FONT_SIZE)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L_FONT_SIZE, "24");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_S_FONT_SIZE)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_S_FONT_SIZE, "18");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L00_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L00_Y, "40");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L01_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L01_Y, "100");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L02_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L02_Y, "160");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L03_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L03_Y, "220");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L04_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L04_Y, "280");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L05_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L05_Y, "340");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_L06_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_L06_Y, "400");
			this.unsaved_changes = true;
		}
		if (!this.preferences.containsKey(PREF_FO_CDU_SCREEN_SCRATCH_Y)) {
			this.preferences.setProperty(PREF_FO_CDU_SCREEN_SCRATCH_Y, "460");
			this.unsaved_changes = true;
		}

		if (this.unsaved_changes) {
			store_preferences();
		}

	}

	private void validate_preferences() {
		// TODO Auto-generated method stub

	}

	/**
	 * @param key
	 *            - the key of the preference
	 * @param value
	 *            - the new value of the preference
	 *
	 * @throws RuntimeException
	 *             - in case key is null or empty
	 */
	public void set_preference(String key, String value) {
		if ((key == null) || (key.trim().equals("")))
			throw new RuntimeException("key must not be null or empty!");
		this.preferences.setProperty(key, value);
		logger.fine("set preference '" + key + "' = '" + value + "'");
		this.unsaved_changes = true;
		store_preferences();
		validate_preferences();
	}

	private void store_preferences() {
		if (this.unsaved_changes) {
			try {
				FileOutputStream fos = new FileOutputStream(PROPERTY_FILENAME);
				preferences.store(fos, null);
				if (fos != null)
					fos.close();
			} catch (IOException e2) {
				logger.warning("Could not store preferences file! (" + e2.toString() + ") ... ");
			}
			this.unsaved_changes = false;
		}
	}

	/**
	 * @param key
	 *            - the key of the preference to be returned
	 * @return - the value of the preference
	 *
	 * @throws RuntimeException
	 *             - in case no preference key is set
	 * @throws RuntimeException
	 *             - in case key is null or empty
	 */
	public String get_preference(String key) {
		return this.preferences.getProperty(key);
	}

	static {
		instance = new ZCDUPreferences();
	}

	public static ZCDUPreferences getInstance() {
		return instance;
	}

}
