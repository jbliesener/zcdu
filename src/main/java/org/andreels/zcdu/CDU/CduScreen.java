/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu.CDU;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
//import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.Timer;

import org.andreels.zcdu.DataFactory;
import org.andreels.zcdu.ZCDUPreferences;
import org.andreels.zcdu.ZCDUStatus;
import org.andreels.zcdu.resources.LoadResources;
import org.andreels.zcdu.xpdata.XPData;


public class CduScreen extends JFrame implements ComponentListener, KeyListener{


	private ZCDUPreferences preferences;
	private DataFactory data_instance;
	private LoadResources rs;
	XPData xpd;
	private Image logo_image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/andreels/zcdu/ZCDU_logo.png"));
	//private static Logger logger = Logger.getLogger("org.andreels.zcdu");

	CduScreenPanel cdu;
	private Color color_magenta = new Color(0xFA7CFA);
	private Color color_cyan = new Color(0x00CCFF);
	private Color color_line00_l;

	private float scalex = 1f;
	private float scaley = 1f;
	private int posX = 0;
	private int posY = 0;
	AffineTransform original_at;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//line 0
	char[] line00_g;
	char[] line00_i;
	char[] line00_l;
	char[] line00_m;
	char[] line00_s;
	char[] line00_x;
	//line 1
	char[] line01_g;
	char[] line01_i;
	char[] line01_l;
	char[] line01_m;
	char[] line01_s;
	char[] line01_x;
	//line 2
	char[] line02_g;
	char[] line02_i;
	char[] line02_l;
	char[] line02_m;
	char[] line02_s;
	char[] line02_x;
	//line 3
	char[] line03_g;
	char[] line03_i;
	char[] line03_l;
	char[] line03_m;
	char[] line03_s;
	char[] line03_x;
	//line 4
	char[] line04_g;
	char[] line04_i;
	char[] line04_l;
	char[] line04_m;
	char[] line04_s;
	char[] line04_x;
	//line 5
	char[] line05_g;
	char[] line05_i;
	char[] line05_l;
	char[] line05_m;
	char[] line05_s;
	char[] line05_x;
	//line 6
	char[] line06_g;
	char[] line06_i;
	char[] line06_l;
	char[] line06_m;
	char[] line06_s;
	char[] line06_x;

	//scratchpad
	char[] sratchpad;

	float l_font = 24f;
	float s_font = 18f;

	private String PREF_POS_X;
	private String PREF_POS_Y;
	private String PREF_WIDTH;
	private String PREF_HEIGHT;

	int pref_posx = 200;
	int pref_posy = 200;

	int pref_cdu_width = 520;
	int pref_cdu_height = 480;

	int line0_y = 40;
	int line1_y = 100;
	int line2_y = 160;
	int line3_y = 220;
	int line4_y = 280;
	int line5_y = 340;
	int line6_y = 400;
	int sratchpad_y = 460;
	

	String title;

	public CduScreen(DataFactory data_instance, String title) {
		this.title = title;
		this.data_instance = data_instance;
		this.xpd = this.data_instance.getInstance();
		this.preferences = ZCDUPreferences.getInstance();
		//set preferences
		if(this.title == "CPT CDU") {
			this.PREF_POS_X = ZCDUPreferences.PREF_CPT_CDU_SCREEN_POSX;
			this.PREF_POS_Y = ZCDUPreferences.PREF_CPT_CDU_SCREEN_POSY;
			this.PREF_WIDTH = ZCDUPreferences.PREF_CPT_CDU_SCREEN_WIDTH;
			this.PREF_HEIGHT = ZCDUPreferences.PREF_CPT_CDU_SCREEN_HEIGHT;
			pref_posx = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_POSX));
			pref_posy = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_POSY));
			pref_cdu_width = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_WIDTH));
			pref_cdu_height = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_HEIGHT));
		}else {
			this.PREF_POS_X = ZCDUPreferences.PREF_FO_CDU_SCREEN_POSX;
			this.PREF_POS_Y = ZCDUPreferences.PREF_FO_CDU_SCREEN_POSY;
			this.PREF_WIDTH = ZCDUPreferences.PREF_FO_CDU_SCREEN_WIDTH;
			this.PREF_HEIGHT = ZCDUPreferences.PREF_FO_CDU_SCREEN_HEIGHT;
			pref_posx = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_POSX));
			pref_posy = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_POSY));
			pref_cdu_width = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_WIDTH));
			pref_cdu_height = Integer.parseInt(this.preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_HEIGHT));
		}

		this.rs = LoadResources.getInstance();
		cdu = new CduScreenPanel();
		this.getContentPane().add(cdu);
		this.setIconImage(this.logo_image);
		this.setTitle(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(pref_posx, pref_posy, pref_cdu_width, pref_cdu_height);
		this.setUndecorated(true);
		JPopupMenu menu = new JPopupMenu("Popup");
		menu.setBackground(Color.DARK_GRAY);
		menu.setBorder(null);
		JLabel popUpTitle = new JLabel(this.title);
		popUpTitle.setForeground(Color.WHITE);
		popUpTitle.setFont(new Font("Tahoma", Font.PLAIN, 16));
		JMenuItem saveLocation = new JMenuItem("Save CDU Size and Location");
		saveLocation.setBackground(Color.GRAY);
		saveLocation.setForeground(Color.WHITE);
		saveLocation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		saveLocation.setToolTipText("Saves the current CDU size and location");
		saveLocation.addActionListener((ActionEvent event) -> {
			this.saveDisplayLocations();
		});
		menu.add(popUpTitle);
		menu.add(new JSeparator());
		menu.add(saveLocation);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				posX = e.getX();
				posY = e.getY();
				if(e.isPopupTrigger()) {
					menu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if(e.isPopupTrigger()) {
					menu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		this.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent evt) {
				setLocation(evt.getXOnScreen()-posX,evt.getYOnScreen()-posY);
			}
		});
		this.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {

					setSize(new Dimension(getWidth() + 2,getHeight() + 2));
				}
				if (e.getWheelRotation() > 0) {
					setSize(new Dimension(getWidth() - 2,getHeight() - 2));
				}	
			} 	
		});
		this.addKeyListener(this);
		this.addComponentListener(this);
		this.setVisible(true);


	}

	private void saveDisplayLocations() {
		int xx = (int) this.getLocationOnScreen().getX();
		int yy = (int) this.getLocationOnScreen().getY();
		this.preferences.set_preference(this.PREF_POS_X, Integer.toString(xx));
		this.preferences.set_preference(this.PREF_POS_Y, Integer.toString(yy));
		this.preferences.set_preference(this.PREF_WIDTH, Integer.toString(this.getWidth()));
		this.preferences.set_preference(this.PREF_HEIGHT, Integer.toString(this.getHeight()));
	}

	public class CduScreenPanel extends JPanel implements ActionListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		Timer updateTimer = new Timer(1000, this);

		public CduScreenPanel() {
			this.setBackground(Color.BLACK);
			updateTimer.setInitialDelay(1);
			updateTimer.setDelay(1000 / 24);
			updateTimer.start();

		}
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			original_at = g2.getTransform();
			
			if(!ZCDUStatus.receiving) {
				
				drawFailCross(g2);
				
			}else if(xpd.power_on()) {
				
				if((title == "CPT CDU" && xpd.power_on()) || (title == "FO CDU" && xpd.dc_standby_status())) {
					
					g2.scale(scalex, scaley);

					if(title == "CPT CDU") {
						l_font = Float.parseFloat(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L_FONT_SIZE));
						s_font = Float.parseFloat(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_S_FONT_SIZE));
						line0_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L00_Y));
						line1_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L01_Y));
						line2_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L02_Y));
						line3_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L03_Y));
						line4_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L04_Y));
						line5_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L05_Y));
						line6_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_L06_Y));
						sratchpad_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_CPT_CDU_SCREEN_SCRATCH_Y));
					}else {
						l_font = Float.parseFloat(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L_FONT_SIZE));
						s_font = Float.parseFloat(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_S_FONT_SIZE));
						line0_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L00_Y));
						line1_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L01_Y));
						line2_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L02_Y));
						line3_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L03_Y));
						line4_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L04_Y));
						line5_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L05_Y));
						line6_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_L06_Y));
						sratchpad_y = Integer.parseInt(preferences.get_preference(ZCDUPreferences.PREF_FO_CDU_SCREEN_SCRATCH_Y));
					}

					//line 0
					line00_g = xpd.cdu_line00_g(title).toCharArray();
					line00_i = xpd.cdu_line00_i(title).toCharArray();
					line00_l = xpd.cdu_line00_l(title).toCharArray();
					line00_m = xpd.cdu_line00_m(title).toCharArray();
					line00_s = xpd.cdu_line00_s(title).toCharArray();
					//String fakeLine00x = " ";
					//line00_x = fakeLine00x.toCharArray();
					//line 1
					line01_g = xpd.cdu_line01_g(title).toCharArray();
					line01_i = xpd.cdu_line01_i(title).toCharArray();
					line01_l = xpd.cdu_line01_l(title).toCharArray();
					line01_m = xpd.cdu_line01_m(title).toCharArray();
					line01_s = xpd.cdu_line01_s(title).toCharArray();
					line01_x = xpd.cdu_line01_x(title).toCharArray();
					//line 2
					line02_g = xpd.cdu_line02_g(title).toCharArray();
					line02_l = xpd.cdu_line02_l(title).toCharArray();
					line02_i = xpd.cdu_line02_i(title).toCharArray();
					line02_s = xpd.cdu_line02_s(title).toCharArray();
					line02_m = xpd.cdu_line02_m(title).toCharArray();
					line02_x = xpd.cdu_line02_x(title).toCharArray();
					//line 3
					line03_g = xpd.cdu_line03_g(title).toCharArray();
					line03_l = xpd.cdu_line03_l(title).toCharArray();
					line03_i = xpd.cdu_line03_i(title).toCharArray();
					line03_s = xpd.cdu_line03_s(title).toCharArray();
					line03_m = xpd.cdu_line03_m(title).toCharArray();
					line03_x = xpd.cdu_line03_x(title).toCharArray();
					//line 4
					line04_g = xpd.cdu_line04_g(title).toCharArray();
					line04_l = xpd.cdu_line04_l(title).toCharArray();
					line04_i = xpd.cdu_line04_i(title).toCharArray();
					line04_s = xpd.cdu_line04_s(title).toCharArray();
					line04_m = xpd.cdu_line04_m(title).toCharArray();
					line04_x = xpd.cdu_line04_x(title).toCharArray();
					//line 5
					line05_g = xpd.cdu_line05_g(title).toCharArray();
					line05_l = xpd.cdu_line05_l(title).toCharArray();
					line05_i = xpd.cdu_line05_i(title).toCharArray();
					line05_s = xpd.cdu_line05_s(title).toCharArray();
					line05_m = xpd.cdu_line05_m(title).toCharArray();
					line05_x = xpd.cdu_line05_x(title).toCharArray();
					//line 6
					line06_g = xpd.cdu_line06_g(title).toCharArray();
					line06_l = xpd.cdu_line06_l(title).toCharArray();
					line06_i = xpd.cdu_line06_i(title).toCharArray();
					line06_s = xpd.cdu_line06_s(title).toCharArray();
					line06_m = xpd.cdu_line06_m(title).toCharArray();
					line06_x = xpd.cdu_line06_x(title).toCharArray();

					//scratchpad
					sratchpad = xpd.cdu_scratchpad(title).toCharArray();

					if(xpd.cdu_line00_l(title).contains("RTE") && !xpd.fpln_active(title)) {
						color_line00_l = color_cyan; 
					}else {
						color_line00_l = Color.WHITE;
					}
					
					
							
					lineBuilder(0, line00_g, line00_i, line00_l, line00_m, line00_s, line00_x, line0_y, g2);
					lineBuilder(1, line01_g, line01_i, line01_l, line01_m, line01_s, line01_x, line1_y, g2);
					lineBuilder(2, line02_g, line02_i, line02_l, line02_m, line02_s, line02_x, line2_y, g2);
					lineBuilder(3, line03_g, line03_i, line03_l, line03_m, line03_s, line03_x, line3_y, g2);
					lineBuilder(4, line04_g, line04_i, line04_l, line04_m, line04_s, line04_x, line4_y, g2);
					lineBuilder(5, line05_g, line05_i, line05_l, line05_m, line05_s, line05_x, line5_y, g2);
					lineBuilder(6, line06_g, line06_i, line06_l, line06_m, line06_s, line06_x, line6_y, g2);
					
					
					g2.setFont(rs.CDUFont.deriveFont(l_font));
					g2.setColor(Color.WHITE);
					for(int i =0; i < sratchpad.length; i++) {
						g2.drawString(String.valueOf(sratchpad[i]), 8 + 19.5f * i, sratchpad_y);
					}
					
					//g2.setTransform(original_at);
					if(xpd.inst_brightness(title) < 1) {
						drawFade(g2);
					}
				}
	
			}

		}

		private void lineBuilder(int lineNumber, char[] g,char[] ii,char[] l,char[] m,char[] s,char[] x, int liney, Graphics2D g2) {
			
			if(g != null) {
				g2.setFont(rs.CDUFont.deriveFont(l_font));
				g2.setColor(Color.GREEN);
				for(int i =0; i < g.length; i++) {
					if(g[i] == '*') {
						g[i] = '!';
					}
					if(g[i] == '`') {
						g[i] = '°';
					}
					g2.drawString(String.valueOf(g[i]), 8 + 19.5f * i, liney);
				}
			}

			if(ii != null) {
				g2.setFont(rs.CDUFont.deriveFont(l_font));
				g2.setColor(Color.WHITE);
				for(int i =0; i < ii.length; i++) {
					if(ii[i] == '*') {
						ii[i] = '!';
					}
					if(ii[i] == '`') {
						ii[i] = '°';
					}
					if(ii[i] != ' ') {
						g2.setColor(Color.LIGHT_GRAY);
						g2.fillRect((int)(8 + 19.5f * i), (int)(liney - 3 - l_font), 21, (int)l_font + 6);	
					}
					g2.setColor(Color.WHITE);
					g2.drawString(String.valueOf(ii[i]), 8 + 19.5f * i, liney);
				}
			}

			if(l != null) {
				g2.setFont(rs.CDUFont.deriveFont(l_font));
				if(lineNumber == 0) {
					g2.setColor(color_line00_l);
				}else {
					g2.setColor(Color.WHITE);
				}
				for(int i =0; i < l.length; i++) {
					if(l[i] == '*') {
						l[i] = '!';
					}
					if(l[i] == '`') {
						l[i] = '°';
					}
					g2.drawString(String.valueOf(l[i]), 8 + 19.5f * i, liney);
				}
			}

			if(m != null) {
				g2.setFont(rs.CDUFont.deriveFont(l_font));
				g2.setColor(color_magenta);
				for(int i =0; i < m.length; i++) {
					if(m[i] == '*') {
						m[i] = '!';
					}
					if(m[i] == '`') {
						m[i] = '°';
					}
					g2.drawString(String.valueOf(m[i]), 8 + 19.5f * i, liney);
				}
			}

			if(s != null) {
				g2.setFont(rs.CDUFont.deriveFont(s_font));
				g2.setColor(Color.WHITE);
				for(int i =0; i < s.length; i++) {
					if(s[i] == '*') {
						s[i] = '!';
					}
					if(s[i] == '`') {
						s[i] = '°';
					}
					g2.drawString(String.valueOf(s[i]), 8f + 19.5f * i, liney);
				}
			}
	
			if(x != null) {
				g2.setFont(rs.CDUFont.deriveFont(s_font));
				g2.setColor(Color.WHITE);
				for(int i =0; i < x.length; i++) {
					if(x[i] == '*') {
						x[i] = '!';
					}
					if(x[i] == '`') {
						x[i] = '°';
					}
					g2.drawString(String.valueOf(x[i]), 8 + 19.5f * i, liney - 30f);
				}
			}

		}
		
		private void drawFailCross(Graphics2D g2) {

			g2.setColor(Color.RED);
			Stroke original_stroke = g2.getStroke();
			g2.setStroke(new BasicStroke(8f * scalex));
			g2.setFont(new Font("Verdana", Font.PLAIN, 20));
			g2.drawString(title, this.getWidth() / 3, this.getHeight() / 9);
			g2.drawString("Width: " + this.getWidth() + ",Height: " + this.getHeight(), this.getWidth() / 3, this.getHeight() / 6.5f);
			g2.drawString("Xpos: " + this.getLocationOnScreen().getX() + ",Ypos: " + this.getLocationOnScreen().getY(),this.getWidth() / 3, this.getHeight() / 5);
			g2.drawLine(0, 0, this.getWidth(), this.getHeight());
			g2.drawLine(this.getWidth(), 0, 0, this.getHeight());
			g2.setStroke(original_stroke);
			g2.dispose();

		}
		
		private void drawFade(Graphics2D g2) {
			
			float fadeValue = 255 * (1 - xpd.inst_brightness(title));
			Color fadeColor = new Color(0, 0, 0, (int) fadeValue);
			g2.setColor(fadeColor);
			g2.fillRect(0, 0, this.getWidth(), this.getHeight());

			
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(this.isVisible()) {
				repaint();
			}


		}

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentResized(ComponentEvent e) {
		scalex = this.getWidth() / 480f;
		scaley = this.getHeight() / 480f;

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			setSize(new Dimension(getWidth() - 1, getHeight()));
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			setSize(new Dimension(getWidth() + 1, getHeight()));
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			setSize(new Dimension(getWidth(), getHeight() - 1));
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			setSize(new Dimension(getWidth(), getHeight() + 1));
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
