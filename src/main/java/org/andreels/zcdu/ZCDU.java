/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu;

import java.awt.EventQueue;
import java.io.File;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.andreels.zcdu.ExtPlaneInterface.ExtPlaneInterface;


public class ZCDU {
	
	DataFactory data_instance;
	private static String ZCDU_VERSION;
	private ZCDUPreferences preferences;

	private static Logger logger = Logger.getLogger("org.andreels.zcdu");
	
	public ZCDU () throws Exception {

		Handler handler = new ConsoleHandler();
		handler.setLevel(Level.ALL);
		handler.setFormatter(new ZCDULogFormatter());
		handler.setFilter(null);
		logger.addHandler(handler);
		
		String strTmp = System.getProperty("java.io.tmpdir");
		File tempDir = new File(strTmp);
		if (tempDir.exists()) {
			File tempFile = new File(tempDir, "ZCDU.log");
			handler = new FileHandler(tempFile.getCanonicalPath());
			handler.setLevel(Level.ALL);
			handler.setFormatter(new ZCDULogFormatter());
			handler.setFilter(null);
			logger.addHandler(handler);
		}

		logger.setLevel(Level.ALL);
		logger.setUseParentHandlers(false);

		ZCDU_VERSION = getVersion();
		
		logger.info("ZCDU version " + ZCDU_VERSION + " started");

		Runtime java_run = Runtime.getRuntime();
		logger.config("JAVA version: " + System.getProperty("java.version"));
		logger.config("Free  Memory: " + (java_run.freeMemory()/1024/1024) + "M");
		logger.config("Total Memory: " + (java_run.totalMemory()/1024/1024) + "M");
		logger.config("Max  Memory: " + (java_run.maxMemory()/1024/1024) + "M");

		ZCDUStatus.status = ZCDUStatus.STATUS_STARTUP;
		
		// load properties and create a new properties file, if none exists
		this.preferences = ZCDUPreferences.getInstance();
		logger.config("Selected loglevel: " + this.preferences.get_preference(ZCDUPreferences.PREF_LOGLEVEL));
		logger.setLevel(Level.parse(this.preferences.get_preference(ZCDUPreferences.PREF_LOGLEVEL)));
		data_instance = new XPlaneDataFactory();
		//creates a new ExtPlaneInterface instance if not already created
		ExtPlaneInterface iface = ExtPlaneInterface.getInstance();
		
		Thread ext_iface = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					iface.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		ext_iface.start();
		
		//finally start the UI
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI ui = new UI(data_instance, "ZCDU version: " + ZCDU_VERSION);
					ui.setLocationRelativeTo(null);
					ui.setVisible(true);
//					if(preferences.get_preference(ZHSIPreferences.PREF_START_MINIMIZED).equals("true")) {
//						ui.setState(Frame.ICONIFIED);
//					}
					UIHeartbeat ui_heartbeat = new UIHeartbeat(ui, 500);
					ui_heartbeat.start();
					//running_threads.add(ui_heartbeat);
					logger.info("Starting UI heartbeat");

					InterfaceHeartbeat if_heartbeat = new InterfaceHeartbeat(500, data_instance);
					if_heartbeat.start();
					//running_threads.add(if_heartbeat);
					logger.info("Starting interface heartbeat");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	protected String getVersion() {
		try (InputStream installerProperties = ZCDU.class.getResourceAsStream("/ZCDU.properties")) {
			Properties props = new Properties();
			props.load(installerProperties);
			String version = props.getProperty("ZCDU.version");
			if (version == null) {
				return "";
			} else {
				return version;
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "Can't recover version number: ", e);
			return "";
		}
	}



	public static void main(String[] args) throws Exception {
	
		new ZCDU();

	}

}
