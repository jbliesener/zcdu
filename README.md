# ZCDU (Zibo737 CDU)

> ZCDU is a external CDU screen display for Zibo737

![](header.png)

## Requirements

This software uses the [ExtPlane Plugin](https://github.com/vranki/ExtPlane), so please make sure you have that plugin installed first.

## Usage example

## Release History
* 0.0.1d: Reconnect after X-Plane shutdown, fix new ExtPlane data format, new build environment

* 0.0.1: Work in progress
   
## Meta
Jorg Neves Bliesener - [mailto:jbliesener@bliesener.com](mailto:jbliesener@bliesener.com)
Andre Els – [https://www.facebook.com/sum1els737](https://www.facebook.com/sum1els737)

Distributed under the GNU General Public License. See [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/) for more information.